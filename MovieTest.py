# -*- coding: utf-8 -*-

"""
MovieTest.py - Simple toy python code.

    Allows me to grab some information from video files using ffprobe's output

    Copyright (C) 2018  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of version 3 of the GNU General Public License
    as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""

import sys
import logging

from os import path
from math import gcd
from json import load as json_load
from argparse import ArgumentParser

from tempfile import NamedTemporaryFile, _TemporaryFileWrapper
from pathlib import Path

from ffmpy import FFprobe

from Filmfile import Filmfile, UnhandledStreamException


class MovieTest:
    def __init__(self, parser: ArgumentParser) -> None:

        films = Filmfile()

        # Populate the filename list in the Filmfile instance
        # noinspection PyTypeChecker
        parser.parse_args(namespace=films)

        if films.debug:

            log_level = logging.DEBUG

        else:

            log_level = logging.INFO

        logging.basicConfig(level=log_level, format="%(levelname)s - %(message)s")

        executable = Path(films.executable)

        logging.debug("executable: %s", executable)

        if not executable.exists():

            logging.error("ffprobe command given doesn't exist!")

            sys.exit(127)

        for fname in range(len(films.filename)):

            logging.debug("films: %s", films.filename[fname])

            expanded = Path(films.filename[fname]).resolve()

            logging.debug("expanded: %s", expanded)

            safename = expanded

            # Get safename's extension
            filetype = safename.suffix[1:].lower()

            logging.debug("safename: %s", safename)

            if not expanded.exists() or path.getsize(safename) == 0:

                logging.info("%s doesn't exist, or is zero-length!?", safename)

                continue

            if expanded.is_dir():

                logging.info("path %s needs a wildcard extension!", safename)

                continue

            temp = self.do_ffprobe_call(safename, executable)

            with open(temp.name, "r") as fp:

                json_obj = json_load(fp)

                stream_no = 0

                file_format = json_obj.get("format")

                logging.debug("Format: %s", file_format)

                if file_format is None:

                    continue

                no_streams = file_format.get("nb_streams", 0)

                logging.debug("fname: %s, no_streams: %s", fname, no_streams)

                films.set_no_streams(index=fname, num=no_streams)

                films.set_duration(index=fname, duration=file_format.get("duration"))

                while stream_no < no_streams:

                    streams = json_obj.get("streams")
                    fformat = json_obj.get("format")

                    stream_tags = streams[stream_no].get("tags", None)

                    codec_type = streams[stream_no].get("codec_type")

                    logging.debug("codec_type: %s", codec_type)

                    codec = streams[stream_no].get("codec_name")

                    bit_rate = streams[stream_no].get("bit_rate", None)

                    width = 0
                    height = 0
                    dar = None

                    logging.debug("streams[%d]: %s", stream_no, streams[stream_no])

                    if codec_type == "video":

                        width = streams[stream_no].get("width")
                        height = streams[stream_no].get("height")
                        dar = streams[stream_no].get("display_aspect_ratio")

                        if dar is not None:

                            # Fix interesting display aspect ratios
                            a, b = dar.split(":")

                            a = float(a)
                            b = float(b)

                            if a == 160.0:

                                a, b = int(a / 12.5), int(b / 12.5)

                                dar = f"{a}:{b}"

                        # Apparently the video stream doesn't always
                        #  have a display_aspect_ratio, so calculate
                        #  it instead for output later.
                        else:

                            logging.debug("Calculating DAR.")

                            denom = gcd(width, height)

                            wr = int(width / denom)
                            hr = int(height / denom)

                            dar = f"{wr}:{hr}"

                        if filetype not in ("mp3", "ogg",):

                            films.set_dimensions(
                                index=fname, dimensions=f"{width}x{height} ({dar})"
                            )

                    check_bitrate = None
                    language = None
                    comment = ""

                    if stream_tags is not None:

                        check_bitrate = stream_tags.get("BPS-eng", None)
                        language = stream_tags.get("language", None)
                        comment = stream_tags.get("comment", "")

                    if bit_rate is not None:

                        # Make sure to use 1000s separator
                        bitrate = self.__format_bitrate__(bit_rate)

                        logging.debug("stream bitrate: %s", bitrate)

                        films.set_has_stream_bitrate(fname, True)
                        films.set_overall_bitrate(fname, None)

                    # MKV files have a tag that holds the bitrate for the stream
                    else:

                        if check_bitrate is not None:

                            # Make sure to use 1000s separator
                            bitrate = self.__format_bitrate__(check_bitrate)

                            logging.debug("stream (check_)bitrate: %s", bitrate)

                            films.set_has_stream_bitrate(fname, True)
                            films.set_overall_bitrate(fname, None)

                        else:  # Fallback on overall bitrate

                            films.set_has_stream_bitrate(fname, False)

                            bitrate = self.__format_bitrate__(fformat.get("bit_rate"))

                            films.set_overall_bitrate(fname, f"{bitrate} kb/s")

                    profile = streams[stream_no].get("profile", "Unknown")

                    format_name = fformat.get("format_name")

                    if format_name.startswith("matroska"):

                        out_value = ""

                        if codec_type == "audio":

                            if language is not None and language != "und":

                                out_value = (
                                    out_value
                                    + f"[{stream_no}] {codec_type.capitalize()} [{language}]    "
                                )
                                out_value += f": {codec}({profile})"

                                if films.get_has_stream_bitrate(fname):

                                    out_value += f": {bitrate} kb/s"

                            else:

                                out_value = (
                                    out_value
                                    + f"[{stream_no}] {codec_type.capitalize()}            : {codec}({profile})"
                                )

                                if films.get_has_stream_bitrate(fname):

                                    out_value += f": {bitrate} kb/s"

                        elif codec_type == "video":

                            out_value = (
                                out_value + f"[{stream_no}] {codec_type.capitalize()} "
                            )

                            if language is not None:

                                out_value += f"[{language}]    : "

                            else:

                                out_value += "         : "

                            out_value += f"{codec}({profile})"

                            if films.get_has_stream_bitrate(fname):

                                out_value += f": {bitrate} kb/s"

                        elif codec_type == "subtitle":

                            out_value = (
                                out_value
                                + f"[{stream_no}] {codec_type.capitalize()} [{language}] : {codec}({profile})"
                            )

                        else:

                            out_value = ""

                    # Not a Matroska container (format)
                    else:

                        films.set_overall_bitrate(fname, None)
                        films.set_has_stream_bitrate(fname, True)

                        if codec_type == "video":

                            if language is not None and language != "und":

                                out_value = f"[{stream_no}] {codec_type.capitalize()} [{language}] : {codec}({profile}): {bitrate} kb/s"

                            else:
                                out_value = f"[{stream_no}] {codec_type.capitalize()}       : {codec}({profile}): {bitrate} kb/s"

                            format_name = file_format.get("format_name", "")

                            # Cover images are encoded as video streams,
                            #  which we want to ignore.
                            if "Cover" in comment or format_name.lower() == "mp3":

                                out_value = ""

                        elif codec_type == "audio":

                            if language is not None and language != "und":

                                out_value = f"[{stream_no}] {codec_type.capitalize()} [{language}] : {codec}({profile}): {bitrate} kb/s"

                            else:

                                out_value = f"[{stream_no}] {codec_type.capitalize()}       : {codec}({profile}): {bitrate} kb/s"

                        elif codec_type == "subtitle":

                            out_value = f"[{stream_no}] {codec_type.capitalize()} [{language}] : {codec}({profile})"

                        else:

                            out_value = ""

                    films.set_streams(
                        index=fname, stream=stream_no, value=out_value.strip()
                    )

                    stream_no += 1

        for ind in range(len(films.filename)):

            # Only print out info for valid filenames
            if films.get_no_streams(index=ind) > 0:

                print(f"Filename:   '{films.get_filename(index=ind)}'")
                print(f"Duration:   {films.get_duration(index=ind)}")

                film_dimensions = films.get_dimensions(index=ind)

                if film_dimensions != "":
                    print(f"Dimensions: {film_dimensions}")

                print()

                overall_bitrate = films.get_overall_bitrate(index=ind)

                if overall_bitrate is not None:

                    print(f"Overall Bitrate: {overall_bitrate}")

                for xe in range(films.get_no_streams(index=ind)):

                    try:

                        print(f"{films.get_streams(index=ind, stream=xe)}")

                    except (UnhandledStreamException, KeyError):

                        continue

                print()

    def do_ffprobe_call(
        self, safename: Path, executable: Path
    ) -> _TemporaryFileWrapper:

        temp = NamedTemporaryFile()

        logging.debug("temp: %s", temp.name)

        options = "-print_format json -show_format -show_streams"

        g_options = "-v quiet -hide_banner"

        logging.debug(
            "command-line: %s -i %s %s %s", executable, safename, options, g_options
        )

        ffprobe = FFprobe(
            executable=executable,
            inputs={
                safename: options,
            },
            global_options=g_options,
        )

        ffprobe.run(stdout=temp)

        return temp

    @staticmethod
    def __format_bitrate__(bitrate: str) -> str:

        return f"{(int(round((float(bitrate) / 1000), 0))):,}"
