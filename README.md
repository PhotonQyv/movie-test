Toy python app that uses ffprobe (local version specified in MovieTest.py) 
to get the audio and video stream information from video files.

I wrote it to enable me to quickly see if a given video would need re-encoding 
before it could be Gnomecast to my Chromecast.

My Chromecast accepts video: h264 (High) with audio: aac (LC) for example. 

Gnomecast knows what is acceptable, and only tries to re-encode
files that don't meet the Chromecast's requirements.

To save time, I wanted to make sure that any video I wanted to 'cast would 
already be suitable, hence this toy application.

NB. If it turns out the video cannot be 'cast as-is I just use ffmpeg on it 
to convert it to what I need.

PhotonQyv (:*
