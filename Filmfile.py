# -*- coding: utf-8 -*-

from dataclasses import dataclass

"""
Filmfile.py - Simple toy python code to allow me to grab some
                 information from video files using ffprobe's output

    Copyright (C) 2018  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


class UnhandledStreamException(Exception):

    """Raised when we have a stream that we don't currently handle"""


# Class used to store the filenames got from the command-line,
#  and then used to store all the other relevant info for each
#  file checked by ffprobe


@dataclass
class Filmfile:
    def __init__(self):

        self.filename: list[str] = list()
        self.no_streams: dict[int, int] = dict()
        self.streams: dict[int, int] = dict()
        self.duration: dict[int, float] = dict()
        self.dimensions: dict[int, str] = dict()
        self.has_stream_bitrate: dict[int, bool] = dict()
        self.overall_bitrate: dict[int, str] = dict()
        self.debug: bool
        self.executable: str

    def get_filename(self, index: int) -> str:

        return self.filename[index]

    def set_no_streams(self, index: int, num: int):

        self.no_streams[index] = num

    def get_no_streams(self, index: int) -> int:

        try:

            return self.no_streams[index]

        except KeyError:

            return 0

    def set_overall_bitrate(self, index: int, overall_bitrate: str):

        self.overall_bitrate[index] = overall_bitrate

    def get_overall_bitrate(self, index: int):

        return self.overall_bitrate[index]

    def set_has_stream_bitrate(self, index: int, stream_has_bitrate: bool):

        self.has_stream_bitrate[index] = stream_has_bitrate

    def get_has_stream_bitrate(self, index: int):

        return self.has_stream_bitrate[index]

    def set_streams(self, index: int, stream: int, value: str):

        self.streams[index, stream] = value

    def get_streams(self, index: int, stream: int) -> str:

        the_stream = self.streams[index, stream]

        if the_stream != "":

            return the_stream

        else:

            raise UnhandledStreamException(
                "We don't currently care about anything other than video, "
                + "audio, or subtitle streams."
            )

    def get_duration(self, index: int) -> str:

        ret_str = ""
        h_txt = m_txt = s_txt = ""

        seconds = self.duration[index]

        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)

        hours = int(hours)
        minutes = int(minutes)
        seconds = int(seconds)

        if hours > 0:
            if hours > 1:
                h_txt = "hrs"
            else:
                h_txt = "hr"

            ret_str += f"{hours} {h_txt} "

        if minutes > 0:
            if minutes > 1:
                m_txt = "mins"
            else:
                m_txt = "min"

            ret_str += f"{minutes} {m_txt} "

        if seconds > 0:
            if seconds > 1:
                s_txt = "secs"
            else:
                s_txt = "sec"

            ret_str += f"{seconds} {s_txt}"

        return ret_str

    def set_duration(self, index: int, duration: str):

        self.duration[index] = float(duration)

    def set_dimensions(self, index: int, dimensions: str):

        self.dimensions[index] = dimensions

    def get_dimensions(self, index: int) -> str:

        dimensions = ""

        if len(self.dimensions) > 0 and index in self.dimensions:

            dimensions = self.dimensions[index]

        return dimensions
